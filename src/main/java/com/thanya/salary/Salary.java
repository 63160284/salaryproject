/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.salary;

/**
 *
 * @author Thanya
 */
public class Salary {
    private int hour;
    private double wage;
    public Salary(int hour, double wage){
        this.hour = hour;
        this.wage = wage;
    }
    public double totalSalary(){
        return hour*wage;
    }
    public void setH(int hour){
        this.hour = hour;
      
    }
    
}
